# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [2.0.0] 2020-06-24

### Added

* Introduced several environment variables using python-dotenv (`.env` file). The following variables should be set before running:
    * `station-api` - The address where the station monitoring service API is exposed. This is where it will submit the measurement reports to. Example: https://raspberrypi-main.local:8080
    * `station-name` - The name under which the station is known to the station monitoring service. Example: my-test-station
    * `auth0-domain` - This is where the script will authenticate itself and obtain an access token from. Default: rcm.eu.auth0.com
    * `client-id` - The client ID used for authentication. 
    * `client-secret` - The secret used for authentication.
    * `audience` - Specify the audience to which the authenticated request will be made. This should be validated by the station monitoring service. Default: https://station-monitoring-service
* Introduced M2M authentication using Auth0. 

### Changed
Default log level changed from debug to info. 

### Removed
Previously the script accepted two arguments to be passed to the script, these are now moved to the environment variables.

## [1.0.0] 2020-02-10

Initial release of the reader for the SHT31-D sensor. Submits temperature (°C) and humidity (%) to the station-monitoring-service.

[2.0.0]: https://gitlab.com/residential-climate-monitoring/sht31-d/-/releases/rcm-sht31-d-reader-2.0.0
[1.0.0]: https://gitlab.com/residential-climate-monitoring/sht31-d/-/releases/rcm-sht31-d-reader-1.0.0
