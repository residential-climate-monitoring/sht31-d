# SHT31-D - High precision temperature and humidity sensor

Python module that reads the [Adafruit SHT31-D] sensor and posts the readings to the station monitoring service API.

# Prerequisites

## Install and update raspbian and python

Make sure the pi is running the latest version of Raspbian and Python 3:

```shell script
sudo apt-get update
sudo apt-get upgrade
sudo pip3 install --upgrade setuptools
# If above doesn't work try:
sudo apt-get install python3-pip
```

## Enable I2C

Also make sure I2C is enabled on the Pi. See [enabling I2C]

Tip: for troubleshooting install `i2c-tools` so you can use [i2cdetect]

## Install and run the Station Monitoring Service

Before the sensor can start measurement the sensor and submit a report with the measurements the station monitoring service
should be up and running. Please check the README of the station monitoring service for more details.

---

# Install guide

## Optionally, create a virtual environment:

```shell script
mkdir residential-climate-monitoring && cd residential-climate-monitoring
python3 -m venv .env
source .env/bin/activate
```

## Install python dependencies

Install the following Python libraries:

 
### Install CircutPython:

**Note:** If your default Python is version 3 you may need to run 'pip' instead. Just make sure you aren't trying to use 
CircuitPython on Python 2.x, it isn't supported! 
The instructions below are a short summary of [CircuitPython on Raspberry Pi]

* Install RPI.GPIO:
    This is a prerequisite for the Adafruit CircuitPython library.

    ```shell script
    pip3 install RPI.GPIO
    ```

* Install [Adafruit Blinka Library]:
    This will install the Adafruit CircuitPython library and all the other essential scripts.

    ```shell script
    pip3 install adafruit-blinka
    ```

### Install sensor driver
Install the python library for the [Adafruit SHT31-D] sensor:

```shell script
pip3 install adafruit-circuitpython-sht31d
```
  
### Install requests library
The [Requests library] is used to submit measurements reports to the station monitoring service API.
    
```shell script
pip3 install requests
```
  
### Install python-dotenv library
The [python-dotenv library] is used to read variables from the .env file.
    
```shell script
pip3 install python-dotenv
```

## Install this script
For now, the script is not yet available as a python library that can be installed from PyPi, instead the script will have to be installed manually:

```shell script
git clone https://gitlab.com/residential-climate-monitoring/sht31-d.git
```

## Verify everything is working

Run blinkatest.py to check everything works:

```shell script
python3 sht31-d/rcm-sht31-d-reader/blinkatest.py
```

You should see the following output:
```shell script
Hello blinka!
Digital IO ok!
I2C ok!
SPI ok!
done!
```

---

# Connect the sensor

To connect the sensor using I2C, wire it as follows: Tip: check [Raspberry GPIO] to see where each pin is on the Pi. 

|RPIO         |Sensor    |
|-------------|----------|
|3.3v         |VIN       |
|GND          |GND       |
|GPIO 2 (SDA) |SDA       |
|GPIO 3 (SCL) |SCL       |

By default the I2C address is 0x44, you can change it to 0x45 by connecting *ADR* to a high voltage signal.  

# Configuration

The script requires several environment variables to be set. These variables are read by the script from the `.env` file.
The following variables can be set:

* `station-api` - The address where the station monitoring service API is exposed. This is where it will submit the measurement reports to. Example: https://raspberrypi-main.local:8080
* `station-name` - The name under which the station is known to the station monitoring service. Example: my-test-station
* `auth0-domain` - This is where the script will authenticate itself and obtain an access token from. Default: rcm.eu.auth0.com
* `client-id` - The client ID used for authentication. 
* `client-secret` - The secret used for authentication.
* `audience` - Specify the audience to which the authenticated request will be made. This should be validated by the station monitoring service. Default: https://station-monitoring-service
* `auth-enabled` - Boolean flag to explicitly disable authentication. Defaults to 'true'.
* `log-level` - Set the logging level. Default to 'INFO' when omitted. See [logging levels] for all levels.
* `periodic` - Boolean flag to control [SHT31-D mode].
---

For boolean flags See [distutils strtobool] for all accepted values. 

# A note about authentication

If authentication is enabled, an M2M access token is obtained from the Auth0 server.
This token is stored in the file `token.txt` in the following format:
```
a.b.c
123
```
Where `a.b.c` represents the access token and `123` the timestamp when the token expires.

Every time a measurement report is submitted, the token is retrieved. First it will look for the stored token and return it if as long as it is not expired.
Only when no token was found or it was expired a new token is requested.

# Reading the sensor:

Once all prerequisites are met, the sensor can be read using:

```shell script
python3 sht31-d/rcm-sht31-d-reader/readsensor.py
``` 

It should print something like the following:
```shell script
Reading SHT31-D sensor...
{'temperature': 20.558022430762193, 'humidity': 54.982219983822475}
```

## Reading the sensor on a regular interval

[Crontab] can be used to gather readings from the sensor on a frequent basis.
The benefit of Crontab is that it is easy to configure with a cron expression and will keep working even if the system reboots.

To configure it, enter the following on the terminal:

```shell script
crontab -e
```  
This will open the config file in an editor where the cron expression, and the script to execute are configured.

The following example executes the readsensor.py script once every minute:

```shell script
* * * * * /home/pi/residential-climate-monitoring/.env/bin/python3 /home/pi/residential-climate-monitoring/sht31-d/rcm-sht31-d-reader/readsensor.py
```  

Note that it executes the script with python3 from the virtual env created during the installation.
This virtual env contains all the dependencies the script needs.

---

# Tips

See [Passwordless SSH access] to login to your Raspberry Pi through SSH without typing in your password over and over again.

# Troubleshooting

To troubleshoot, the logs can be found at: `/home/pi/sht31d.log`

# License

The Residential Monitoring Service is distributed under the MIT License

[CircuitPython on Raspberry Pi]: https://learn.adafruit.com/circuitpython-on-raspberrypi-linux
[Enabling I2C]: https://learn.adafruit.com/adafruits-raspberry-pi-lesson-4-gpio-setup/configuring-i2c
[Adafruit SHT31-D]: https://learn.adafruit.com/adafruit-sht31-d-temperature-and-humidity-sensor-breakout
[Requests library]: https://requests.readthedocs.io/en/master/ 
[Adafruit Blinka Library]: https://pypi.org/project/Adafruit-Blinka/
[Raspberry GPIO]: https://www.raspberrypi.org/documentation/usage/gpio/
[Crontab]: https://www.raspberrypi.org/documentation/linux/usage/cron.md
[python-dotenv library]: https://pypi.org/project/python-dotenv/
[distutils strtobool]: https://docs.python.org/3/distutils/apiref.html#distutils.util.strtobool
[logging levels]: https://docs.python.org/3/library/logging.html#logging-levels
[Passwordless SSH access]: https://www.raspberrypi.org/documentation/remote-access/ssh/passwordless.md
[SHT31-D mode]: https://circuitpython.readthedocs.io/projects/sht31d/en/latest/
[i2cdetect]: https://linux.die.net/man/8/i2cdetect
