#  Copyright (c) 2020 Pim Hazebroek
#  This program is made available under the terms of the MIT License.
#  For full details check the LICENSE file at the root of the project.

import distutils.util
import logging
from logging.handlers import RotatingFileHandler, SysLogHandler

import adafruit_sht31d
import board
import busio
import requests
import sys
import time
from dotenv import load_dotenv

CONTENT_TYPE = 'application/json'
LOG_PATH = '/home/pi/sht31d.log'
NEW_LINE = '\n'
SIXTY_SECONDS = 60
READ = 'r'
TOKEN_FILE = 'token.txt'
WRITE = 'w'

load_dotenv()
import os
import os.path

# Import urllib3 and disable warnings about unsecure connection, since HTTPS verify is set to False due to self signed certificate.
import urllib3

urllib3.disable_warnings()


def init_logging():
    """Initializes the logger. The logfile will be rotated when it reaches 1 Mb. Max 5 copies kept as backup.
    :return: a reference to the logger
    """
    logger = logging.getLogger()
    handler = RotatingFileHandler(LOG_PATH, maxBytes=1048576, backupCount=5)
    formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(name)s - %(message)s')
    handler.setFormatter(formatter)
    logger.addHandler(handler)
    logger.setLevel(os.getenv('log-level', 'INFO'))
    return logger


def get_config():
    """Returns the configuration with the (environment) variables needed to run this script.
    Note: follows common naming convention in code as (_) and in config files as (-). """
    config = {
        'station_api': os.getenv('station-api'),
        'station_name': os.getenv('station-name'),
        'auth0_domain': os.getenv('auth0-domain'),
        'client_id': os.getenv('client-id'),
        'client_secret': os.getenv('client-secret'),
        'audience': os.getenv('audience'),
        'auth_enabled': bool(distutils.util.strtobool(os.getenv('auth-enabled', 'true'))),
        'periodic': bool(distutils.util.strtobool(os.getenv('periodic', 'false')))
    }

    if config['auth_enabled'] and (
            config['auth0_domain'] is None
            or config['client_id'] is None
            or config['client_secret'] is None
            or config['audience'] is None):
        raise ValueError("Auth is not configured properly. Either disabled auth explicitly or check the configuration.")

    return config


def get_token():
    """
    Retrieve the access token for M2M authentication on the station monitoring service. Access tokens are 'cached' by writing them to a file locally.
    The file also contains the timestamp when the token expires. If the token was expired a new token will be retrieved.
    Returns None if authentication is disabled.
    """
    if not config['auth_enabled']:
        log.debug('Skipping authentication because it is disabled')
        return None

    if os.path.isfile(TOKEN_FILE):
        with open(TOKEN_FILE, READ) as f:
            content = f.read().splitlines()
            token = content[0]
            expires_at = float(content[1])
        if expires_at > time.time():
            return token
        else:
            log.debug('Token expired')
    else:
        log.debug('Token file does not exist')

    log.debug('Retrieving new token')

    uri = f'https://{config["auth0_domain"]}/oauth/token'
    payload = {
        'grant_type': 'client_credentials',
        'client_id': config['client_id'],
        'client_secret': config['client_secret'],
        'audience': config['audience']
    }

    try:
        response = requests.post(uri, data=payload)
    except requests.exceptions.ConnectionError:
        log.exception("Failed to retrieve access token")
        sys.exit(401)

    if response.status_code != 200:
        log.error('Failed to authenticate. Response: %s', response.text)
        sys.exit(401)

    data = response.json()

    with open(TOKEN_FILE, WRITE) as f:
        token = data['access_token'] + NEW_LINE
        expires_at = str(time.time() + data['expires_in'] - SIXTY_SECONDS)
        f.writelines([token, expires_at])
    return data['access_token']


def submit_report(measurements):
    """Submits the measurement report to the station service API.
        :param measurements a key-value map with the measurements. Each metric has key is with the measuring as value.
    """
    uri = f'{config["station_api"]}/measurement-reports'

    payload = {
        'stationName': config['station_name'],
        'measurements': measurements
    }

    token = get_token()

    headers = {
        'content-type': 'application/json',
        'Authorization': f'Bearer {token}'
    }

    try:
        response = requests.post(uri, json=payload, headers=headers, verify=False)

        if response.status_code != 204:
            log.error('Failed to submit measurement report. Code: %s', response.status_code)
            sys.exit(response.status_code)

        log.debug('Submitted measurement report: %s. station=%s, target=%s', measurements, config['station_name'], uri)
    except requests.exceptions.ConnectionError:
        log.exception("Failed to submit measurements report")
        sys.exit(999)


def read_sensor():
    """Reads the values from the sensor via I2C. In Periodic mode 1 measurement is acquired every 2 seconds."""
    i2c = busio.I2C(board.SCL, board.SDA)
    sensor = adafruit_sht31d.SHT31D(i2c)
    if config['periodic']:
        log.debug('Using periodic mode')
        sensor.mode = adafruit_sht31d.MODE_PERIODIC
        sensor.frequency = adafruit_sht31d.FREQUENCY_0_5

        time.sleep(20)
        avg_temperature = sum(sensor.temperature) / len(sensor.temperature)
        avg_humidity = sum(sensor.relative_humidity) / len(sensor.relative_humidity)

        measurements = {
            'temperature': avg_temperature,
            'humidity': avg_humidity
        }

        # Return to single mode to stop periodic data acquisition and allow it to sleep.
        sensor.mode = adafruit_sht31d.MODE_SINGLE
    else:
        measurements = {
            'temperature': sensor.temperature,
            'humidity': sensor.relative_humidity
        }

    log.info(measurements)
    print(measurements)

    submit_report(measurements)


print('Reading SHT31-D sensor...')
# Initialize logging so we know what is going on
log = init_logging()
log.debug('Reading SHT31-D sensor...')

try:
    # Retrieve the external configuration
    config = get_config()

    # Authenticate against the identity provider and obtain the access token
    # get_token()

    # Actually measure the values of the sensor and send them to the API
    read_sensor()
except:
    print("Unexpected error:", sys.exc_info()[0])
    log.exception("Unexpected error: ", sys.exc_info()[0])
    raise
