#  Copyright (c) 2020 Pim Hazebroek
#  This program is made available under the terms of the MIT License.
#  For full details check the LICENSE file at the root of the project.

import setuptools

with open("README.md", "r") as fh:
  long_description = fh.read()

setuptools.setup(
    name="rcm-sht31-d-reader-phazebroek",
    version="0.0.1",
    author="Pim Hazebroek",
    author_email="author@example.com",
    description="Reads values from a SHT31-D sensor",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/residential-climate-monitoring/sht31-d",
    packages=setuptools.find_packages(),
    classifiers=[
      "Programming Language :: Python :: 3",
      "License :: OSI Approved :: MIT License",
      "Operating System :: OS Independent",
    ],
    python_requires='>=3.6',
)
